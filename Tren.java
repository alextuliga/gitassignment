package git;

public class Tren implements Interfata {
	public String plecare;
	public String destinatie;
	public Tren(String plecare, String destinatie) {
		super();
		this.plecare = plecare;
		this.destinatie = destinatie;
	}
	@Override
	public void trainStatus() {
		System.out.println("Acesta este status-ul trenului");
	}
	
}